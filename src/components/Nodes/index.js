import React from 'react';
import ModalCustom from '../Modal';

const Nodes = (props) => {
    const {
        nodeDom,
        nodeObj,
        deleteNode,
        changeHandler,
        createNode,
        createNodeObj,

    } = props;

    let extraNodes = 0;
    return nodeDom.map((element, index)=>{
    if(element.isNode){
        extraNodes++;
    }
        return (
            <div
                key={index}
                id={element.idContainer}
                className='window'
                data-nodeid={element.idNode}
                data-type={String(element.nodeType)}
                data-objectid={element.id}
                style={{top: element.top, left: element.left}}
            >
                <div className='delete' onClick={(e) => deleteNode(e)}>
                </div>
                <ModalCustom
                    buttonLabel='New Node'
                    nodeObj={nodeObj}
                    changeHandler={changeHandler}
                    nodeType={String(element.nodeType)}
                    createNode={createNode}
                    createNodeObj={createNodeObj}
                    idNode={(element.id - extraNodes)}
                    className='edit'
                />
                <div className='nodeBody'>
                    <p className='msg'>
                        {element.isNode
                            ?
                              (!element.msgInput
                              ? nodeObj[element.parentNode].btns[element.indexBtn].msg
                              : nodeObj[element.parentNode].input.placeholder)
                            :
                            nodeObj[element.id - extraNodes].msg
                        }
                    </p>
                </div>
            </div>
        )
    });
}
export default Nodes;
