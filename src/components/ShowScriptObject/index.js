import React from 'react';
import beautify from 'json-beautify';
import { Input } from 'reactstrap';

const ShowScriptObject = (props) => {
    const {
        nodeObj
  } = props;
  return (
    <Input
      style={{ fontSize:"12px", minHeight: "60vh" }}
      type='textarea'
      value={beautify(nodeObj, null, 2, 20)}
    />
  )
}
export default ShowScriptObject;
