import React, { Component } from 'react';
import ScriptGenerator from './containers/ScriptGenerator';

import './App.css';

class App extends Component {
  constructor() {
    super();

    this.findElement   = this.findElement.bind(this);
    this.sleep         = this.sleep.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.setStateAsync = this.setStateAsync.bind(this);
    this.generateToken = this.generateToken.bind(this);

    this.endpointOptions = {
      endpoint: "Rectangle",
      paintStyle: { width: 15, height: 15, fill: '#666' },
      isSource: true,
      connectorStyle: { stroke: "#666" },
      isTarget: true,
      anchor: ["BottomCenter"]
    };
    this.state={
      numNodes: 0,
      nodeType: 0,
      nodeObj : [],
      nodeDom: [],
      countId: 1,
      newNodes : [],
      numButtons : 0
    }
  }
  findElement = async ( array, elementToFind ) =>{
    return new Promise((resolve) => {
      return resolve(
        array.find((element) => { return element === elementToFind })
      );
    });
  }
  sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
  }
  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }
  generateToken = () => {
    var chars = 'abcdefghijklmnopqrstuvwxyz-0123456789';
    var token = '';
    for (var i = 0; i < 11; i++) {
      token += chars[Math.floor(Math.random() * chars.length)];
    }
    return token;
  }
  changeHandler(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  render() {
    return (
      <div>
        <ScriptGenerator
          endpointOptions = {this.endpointOptions}
          findParents     = {this.findElement}
          sleep           = {this.sleep}
          setStateAsync   = {this.setStateAsync}
          changeHandler   = {this.changeHandler}
          numNodes        = {this.state.numNodes}
          nodeType        = {this.state.nodeType}
          nodeObj         = {this.state.nodeObj}
          nodeDom         = {this.state.nodeDom}
          countId         = {this.state.countId}
          newNodes        = {this.state.newNodes}
          numButtons      = {this.state.numButtons}
          generateToken   = {this.generateToken}
        />
      </div>
    );
  }
}

export default App;
