import React from 'react';
import { Nav, NavItem, Button } from 'reactstrap';

import ModalCustom from '../../components/Modal';

import IosTextOutline from 'react-ionicons/lib/IosTextOutline';
import IosConstructOutline from "react-ionicons/lib/IosConstructOutline";
import desition from '../../img/desition.svg';
import input from '../../img/input.png';

import './style.css';
const Toolbar = (props) => {
    const {
        changeHandler,
        nodeType,
        createNode,
        createNodeObj,
        nodeObj,
        idNode
    } = props;
    return (
        <div>
            <Nav className='justify-content-center'>
                <NavItem className='brd-l'>
                    <Button
                        className='bg-light no-border'
                        onClick={e => createNode(e, false, '1')}
                        >
                        <IosTextOutline/>
                    </Button>
                </NavItem>
                <NavItem>
                    <Button
                        className='bg-light no-border'
                        onClick={e => createNode(e, false, '2')}>
                        <img className='icon-h' src={input} alt='input' />
                    </Button>
                </NavItem>
                <NavItem className='brd-r'>
                    <Button
                        className='bg-light no-border'
                        onClick={e => createNode(e, false, '3')}>
                        <img className='icon-h' src={desition} alt='desition' />
                    </Button>
                </NavItem>
                <NavItem style={{paddingLeft: 5}}>
                    <ModalCustom
                        buttonLabel={<IosConstructOutline/>}
                        nodeObj={nodeObj}
                        changeHandler={changeHandler}
                        nodeType={nodeType}
                        createNode={createNode}
                        createNodeObj={createNodeObj}
                        idNode={idNode}
                    />
                </NavItem>
            </Nav>
        </div>
    );
}

export default Toolbar;
