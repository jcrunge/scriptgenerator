import React, { Component } from 'react';

import { Col, Container, Row } from 'reactstrap';
import { jsPlumb } from 'jsplumb';
import $ from "jquery";
import ShowScriptObject from '../../components/ShowScriptObject';

import Toolbar from '../../components/Toolbar';
import Nodes from '../../components/Nodes';

class ScriptGenerator extends Component {
  constructor(props) {
    super(props);
    this.createNode = this.createNode.bind(this);
    this.nodeObj = this.nodeObj.bind(this);
    this.targetOptions = {
      isTarget: true,
      maxConnections: 5,
      //endpoint: "Rectangle",
      paintStyle: { fill: "gray" },
      anchor: ["Top", "RightMiddle", "LeftMiddle"],
      beforeDrop: async (resp)=>{
        const connection = resp.connection;
        const idNodeEndpoint = parseInt(connection.source.dataset.nodeid);
        const idNodeObjTarget = parseInt(connection.target.dataset.nodeid);
        const nodeObj = [...this.props.nodeObj];
        let nodeTo = null;
        if(connection.source.dataset.type==='1') return true;
        nodeObj.map( async (el) => {
          if(el.id === idNodeObjTarget){
            nodeTo = el
          }
          else if(el.btns !== undefined){
            el.btns.map( (el_btn) => {
              if(el_btn.id === idNodeObjTarget){
                nodeTo = el
              }
              return true;
            })
          }
          else if(el.input !== undefined){
            if(el.input.id === idNodeObjTarget){
              nodeTo = el
            }
          }
        });
        const parteIds = (nodeTo.parentIds)
          ? nodeTo.parentIds
          : 0;
        if (parteIds) {
          const elementExist = await this.props.findParents(parteIds,idNodeEndpoint);
          if (!elementExist)
            nodeTo.parentIds.push(idNodeEndpoint)
        }
        else
          nodeTo.parentIds = [idNodeEndpoint];
        this.props.setStateAsync({
          nodeObj: nodeObj
        });
        return true;
      }
    };
  }

  componentDidMount(){
    jsPlumb.bind("ready", function () {
      jsPlumb.Defaults.Container = $("#container-plumb");
      jsPlumb.importDefaults({ Connector: ["Flowchart"]});
      jsPlumb.on(window, "resize", jsPlumb.repaintEverything);
    });
  }
  componentDidUpdate(){
    this.setNodesPosition();
  }
  nodeObj = async (e, idNode=false, nodeType=0) =>{
    let value = e ? e.target.value : '';
    let name = e ? e.target.name : '';
    let indexNew = this.props.countId;
    const nodeObj = [...this.props.nodeObj];
    let id = idNode !== false ? idNode : nodeObj.length;
    let type = nodeType || this.props.nodeType;
    let newNodes = [...this.props.newNodes];
    if (!nodeObj[id]){

      nodeObj.push({
        id: indexNew
      });
      newNodes.push(indexNew);
      indexNew = indexNew + 1;
      if (name === 'msg'){
        nodeObj[id].msg = value;
      }
    }
    else if (name ==='msg'){
      nodeObj[id].msg = value;
    }
    if (type==='2'){
      if (name === 'validation' && value!=='0'){
        const input = nodeObj[id].input;

        if(!input){

          nodeObj[id].input={
            id: indexNew,
            validation: value
          };
          newNodes.push(indexNew);
          indexNew = indexNew + 1;
        }
        else{
          input.validation = value
        }
      }
      else if (name !== 'msg'){
        const input = nodeObj[id].input;
        if (!input) {

          nodeObj[id].input = {
            id: indexNew,
            [name]: value
          };
          newNodes.push(indexNew);
          indexNew = indexNew + 1;
        }
        else {
          input[[name]] = value
        }
      }
    }
    else if (type === '3'){
      if(name !== 'msg'){
        const btnNum = e ? parseInt(e.target.dataset.btn) : 1;
        const btns = nodeObj[id].btns;
        name = (name === 'msgBtn') ? 'msg' : name;
        if (!btns) {
          let i = 0;
          let newsBtns = [];
          while (i <= btnNum){

            if (btnNum === i){
              newsBtns.push({
                id: indexNew,
                [name]: value
              });
            }
            else{
              newsBtns.push({
                id: indexNew
              });
            }
            newNodes.push(indexNew);
            indexNew = indexNew + 1;
            i++;
          }
          nodeObj[id].btns = newsBtns ;
        }
        else {
          if (btns[btnNum]){
            btns[btnNum][[name]] = value;
          }
          else{
            const newBtn = {
              id: indexNew,
              [name]: value
            }
            btns.push(newBtn);
            newNodes.push(indexNew);
            indexNew = indexNew + 1;
          }
        }
      }
    }
    this.props.setStateAsync({ nodeObj: nodeObj , countId : indexNew, newNodes : newNodes});
  }
  getNodesPosition = async () =>{
    let currNodes = document.getElementsByClassName("window");
    let nodes = Array.from(currNodes)
    let nodeDom = [...this.props.nodeDom];
    await nodes.map( async (node, index) =>{
      let objectid = parseInt(node.dataset.objectid);
      nodeDom[objectid]["top"] = node.offsetTop;
      nodeDom[objectid]["left"] = node.offsetLeft;
      return true;
    });
    this.setState({nodeDom : nodeDom})
  }

  setNodesPosition = async () => {
    let currNodes = document.getElementsByClassName("window");
    let nodes = Array.from(currNodes)
    let nodeDom = [...this.props.nodeDom];
    await nodes.map( async (node, index) =>{
      let objectid = parseInt(node.dataset.objectid);
      node.style.top = nodeDom[objectid]["top"];
      node.style.left = nodeDom[objectid]["left"];
      return true;
    });
  }
  createNode = async (e,toogleModal=false, createNodeType=false)=>{
    await this.getNodesPosition();
    if(toogleModal)
      toogleModal();
    else{
      await this.props.setStateAsync({ nodeType: createNodeType });
      this.nodeObj();
    }
    let newNodes = [...this.props.newNodes];
    const nodeType = this.props.nodeType;
    const nodes = [...this.props.nodeObj];
    let numButtons = this.props.numButtons;
    let i = this.props.numNodes;
    let parentNode = null;
    if (nodeType && nodeType!=='0'){
      let nodeDom = this.props.nodeDom;
      const idContainer = this.props.generateToken();
      nodeDom.push({
        id: i,
        idContainer: idContainer,
        isNode: false,
        parentNode : false,
        idNode: newNodes[0],
        nodeType: nodeType,
        top : 50,
        left : 50
      });
      if(newNodes.length > 1){
        parentNode = nodes.length - 1;
      }
      newNodes.splice(0, 1);
      await this.props.setStateAsync({
        nodeDom: nodeDom
      })
      await this.props.sleep(800);
      const findAgent = document.getElementById(idContainer);
      //this.createTypeNode(nodeType, findAgent); //style
      jsPlumb.revalidate(findAgent);
      jsPlumb.makeTarget(findAgent, this.targetOptions);
      jsPlumb.draggable(findAgent);
      //await this.props.sleep(800);

      if (nodeType!=='3'){
        if(nodes[nodes.length - 1].input)
        {
          numButtons = numButtons + 1;
          const idContainer = this.props.generateToken();
          nodeDom.push({
            id: i +  1,
            idContainer: idContainer,
            isNode: true,
            msgInput: true,
            parentNode : parentNode,
            idNode: newNodes[0],
            nodeType: nodeType,
            top : 200,
            left : 50
          });
          newNodes.splice(0, 1);
          await this.props.setStateAsync({
            nodeDom: nodeDom
          });
          await this.props.sleep(500);
          const newAgentBtn = document.getElementById(idContainer);
          //this.createTypeNode(nodeType, newAgentBtn); //style
          jsPlumb.addEndpoint(newAgentBtn, this.props.endpointOptions);
          jsPlumb.connect({
            source: findAgent,
            target: newAgentBtn,
            anchors: [["Bottom"],["Top", "RightMiddle", "LeftMiddle"]]
          });
          jsPlumb.draggable(newAgentBtn);
        }
        else{
          jsPlumb.addEndpoint(findAgent, this.props.endpointOptions);
        }
      }
      else{
        if (nodes[nodes.length - 1].btns.length>0){
          numButtons = numButtons + nodes[nodes.length - 1].btns.length;
          nodes[nodes.length - 1].btns.forEach(async (element, index) => {
            const idContainer = this.props.generateToken();
            nodeDom.push({
              id: i + index + 1,
              idContainer: idContainer,
              isNode: true,
              parentNode : parentNode,
              idNode: newNodes[0],
              nodeType: nodeType,
              indexBtn: index,
              top : 200,
              left : 50
            });
            newNodes.splice(0, 1);
            await this.props.setStateAsync({
              nodeDom: nodeDom
            });
            await this.props.sleep(500);
            const newAgentBtn = document.getElementById(idContainer)
            //this.createTypeNode(nodeType, newAgentBtn); //style
            await this.props.sleep(200);
            jsPlumb.makeTarget(newAgentBtn, this.targetOptions);
            jsPlumb.addEndpoint(newAgentBtn, this.props.endpointOptions);
            jsPlumb.connect({
              source: findAgent,
              target: newAgentBtn
            }, { anchor: ["Top", "RightMiddle", "LeftMiddle"]});
            jsPlumb.draggable(newAgentBtn);

          });
        }
      }
      let sumNewNodes = 0;
      if(nodes[nodes.length - 1].btns){
        sumNewNodes = nodes[nodes.length - 1].btns.length;
      }
      else if(nodes[nodes.length - 1].input){
        sumNewNodes++;
      }
      await this.props.setStateAsync({
        numNodes: i + 1 + sumNewNodes,
        nodeType: 0,
        newNodes : [],
        numButtons : numButtons
      });
    }
  }
  deleteNode = async (element) =>{
    let node = element.target.offsetParent;
    let nodes = [...this.props.nodeObj];
    let numButtons = this.props.numButtons;
    let numNodes = this.props.numNodes;
    let nodesCount = 0;
    let indexDomToDelete = null;
    const nodeToDelete = parseInt(node.dataset.nodeid)
    let elementToDelete = null;
    let deleted = false;
    let idsToDelete = [];
    let isbtn = false;
    let isNode = false;
    await this.getNodesPosition();
    nodes.map(async (el, index) => {
      let actualNode = false;
      if (el.id === nodeToDelete) {
        elementToDelete = index;
        actualNode = true;
        idsToDelete.push(el.id);
        isNode = true;
        numNodes--;
        indexDomToDelete = index + nodesCount;
      }
      if (el.btns) {
        let buttons = el.btns;
        buttons.map(async (btn_el, btn_index) => {
          nodesCount++;
          if (elementToDelete !== null && isNode && !isbtn && actualNode) {
            idsToDelete.push(btn_el.id);
            numButtons--;
            numNodes--;
          }
          else if (btn_el.id === nodeToDelete) {
            isbtn = true;
            elementToDelete = btn_index;
            idsToDelete.push(btn_el.id);
            numButtons--;
            numNodes--;
          }
        });
        if (elementToDelete !== null && isbtn) {
          actualNode = true;
          buttons.splice(elementToDelete, 1);
          indexDomToDelete = index + nodesCount;
          if(buttons.length) {
            nodes[index]["btns"] = buttons;
          }
          else{
            delete nodes[index]["btns"]
          }
          deleted = true;
        }
      }
      if(el.input){
        nodesCount++;
        if(actualNode){
          numButtons--;
          numNodes--;
          idsToDelete.push(el.input.id);
        }
        if (el.input.id === nodeToDelete) {
          numButtons--;
          numNodes--;
          idsToDelete.push(el.input.id);
          delete nodes[index]["input"];
          deleted = true;
        }
      }
    });
    if (!deleted && elementToDelete !== null && !isbtn) {
      nodes.splice(elementToDelete, 1);
      deleted = true;

    }
    let nodeDom = [...this.props.nodeDom];
    if (deleted) {

      //jsPlumb.remove(node);
      nodes = this.deleteParentsIds(idsToDelete, nodes);
      let deletedNodes = 0;
      let domSize = nodeDom.length;
      let domPointer = 0;
      let buttnsRemoved = 0;
      let nodesToDelete = [];
      nodeDom.map(async (el, index) => {
        if(idsToDelete.includes(el.idNode)){
          nodesToDelete.push(index);
        }
      });
      let sonRemovedParent = null
      while (domPointer < domSize){
        if(nodesToDelete.includes(domPointer + deletedNodes)){
          if(nodeDom[domPointer] !== undefined){
            //console.log(nodeDom[domPointer].id);
            //jsPlumb.remove(nodeDom[domPointer].id);
            jsPlumb.removeAllEndpoints(nodeDom[domPointer].idContainer);
            if(nodeDom[domPointer]["parentNode"] !== false){
              sonRemovedParent = nodeDom[domPointer]["parentNode"];
              buttnsRemoved++;
            }
            //console.log(domPointer, "before splice");
            nodeDom.splice(domPointer, 1);
            //console.log(nodeDom, "after splice");
            domPointer--;
            deletedNodes++;

          }
        }
        else{
          if(nodeDom[domPointer] !== undefined) {
            if(indexDomToDelete <= domPointer + deletedNodes){
              nodeDom[domPointer]["id"] = nodeDom[domPointer]["id"] - deletedNodes;
              if(nodeDom[domPointer]["parentNode"] !== false){
                if(sonRemovedParent === nodeDom[domPointer]["parentNode"]){
                  nodeDom[domPointer]["indexBtn"] = nodeDom[domPointer]["indexBtn"] - buttnsRemoved;
                }
                else {
                  nodeDom[domPointer]["parentNode"] = nodeDom[domPointer]["parentNode"] - deletedNodes + buttnsRemoved;
                }

              }
            }
          }
        }
        domPointer++;
      }
      //jsPlumb.repaintEverything();
      //console.log(nodeDom);
      this.props.setStateAsync({ nodeDom: nodeDom, nodeObj: nodes, numButtons : numButtons, numNodes: numNodes });
    }
  }
  deleteParentsIds = (ids, nodes) => {
    nodes.map(async (el, index) => {
      let parents = el.parentIds;
      if (parents) {
        parents.map(async (el, index) => {
          if (ids.includes(el)) {
            parents.splice(index, 1);
          }
        });
        nodes[index]["parentIds"] = parents;
        if (el.btns) {
          let buttons = el.btns;
          buttons.map(async (btn_el, index_el) => {
            parents = btn_el.parentIds;
            if (parents) {
              parents.map(async (el, index) => {
                if (ids.includes(el)) {
                  parents.splice(index, 1);
                }
              });
              nodes[index]["btns"][index_el]["parentIds"] = parents;
            }
          });
        }
      }
    });
    return nodes;
  }
  render() {
    return (
      <>
        <Toolbar
          nodeObj={this.props.nodeObj}
          createNode={this.createNode}
          idNode={this.props.numNodes - this.props.numButtons}
          createNodeObj={this.nodeObj}
          changeHandler={this.props.changeHandler}
          nodeType={this.props.nodeType}
        />
        <Container>
          <Row>
            <Col sm={10} md={10} xs={12} className='d-sm-block'>
              <div id='container-plumb'>
                <Nodes
                  nodeDom={this.props.nodeDom}
                  nodeObj={this.props.nodeObj}
                  deleteNode={this.deleteNode}
                  changeHandler={this.props.changeHandler}
                  createNode={this.createNode}
                  createNodeObj={this.nodeObj}
                />
              </div>
            </Col>
            <Col sm={2} md={2} className='d-none d-sm-block' id='jsonRender'>
              <ShowScriptObject
                nodeObj={this.props.nodeObj}
              />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}
export default ScriptGenerator;