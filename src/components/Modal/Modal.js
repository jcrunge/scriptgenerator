import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import MdCreate from 'react-ionicons/lib/MdCreate';

const ModalCustom = (props) => {
    const {
        buttonLabel,
        className,
        changeHandler,
        nodeType,
        createNode,
        createNodeObj,
        nodeObj,
        idNode
    } = props;

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const renderTypeNode = () => {
        if (nodeType==='1'){
            return(
                <Input
                    type='text'
                    name='msg'
                    placeholder='Type your message'
                    value={(nodeObj && nodeObj[idNode]) ? nodeObj[idNode].msg : ''}
                    onChange={(e) => createNodeObj(e, idNode)}
                >
                </Input>
            )
        }
        else if(nodeType==='2'){
            const input = (nodeObj && nodeObj[idNode]
                && nodeObj[idNode].input)
                ? nodeObj[idNode].input
                : false;
            return (
                <>
                    <Input
                        type='text'
                        name='msg'
                        placeholder='Type your message'
                        value={(nodeObj && nodeObj[idNode]) ? nodeObj[idNode].msg : ''}
                        onChange={(e) => createNodeObj(e, idNode, nodeType)}
                    >
                    </Input>
                    <Input
                        type='select'
                        name='validation'
                        defaultValue={input && input.validation ? nodeObj[idNode].input.validation : '0'}
                        onChange={(e) => createNodeObj(e, idNode, nodeType)}
                    >
                        <option value="0">
                            No validation
                        </option>
                        <option value="ProfanityCheck">
                            ProfanityCheck
                        </option>
                        <option value="email">
                            Email
                        </option>
                        <option value="phone">
                            Phone
                        </option>
                    </Input>
                    <Input
                        value={input && input.saveAsKey ? nodeObj[idNode].input.saveAsKey : ''}
                        type='text'
                        name='saveAsKey'
                        placeholder='Save As Key'
                        onChange={(e) => createNodeObj(e, idNode, nodeType)}>
                    </Input>
                    <Input
                        value={input && input.placeholder ? nodeObj[idNode].input.placeholder : ''}
                        type='text'
                        name='placeholder'
                        placeholder='placeholder name'
                        onChange={(e) => createNodeObj(e, idNode, nodeType)}
                    >
                    </Input>
                    <Input
                        value={input && input.cssClass ? nodeObj[idNode].input.cssClass : ''}
                        type='text'
                        name='cssClass'
                        placeholder='CSS class'
                        onChange={(e) => createNodeObj(e, idNode, nodeType)}>
                    </Input>
                </>
            )
        }
        else if (nodeType === '3'){
            const btns = (nodeObj && nodeObj[idNode]
                && nodeObj[idNode].btns)
                ? nodeObj[idNode].btns
                : false;
            return (
            <>
                <Input
                    value={(nodeObj && nodeObj[idNode]) ? nodeObj[idNode].msg : ''}
                    type='text'
                    name='msg'
                    placeholder='Type your message'
                    onChange={(e) => createNodeObj(e, idNode, nodeType)}
                />
                <Input
                    type='text'
                    data-btn='0'
                    name='msgBtn'
                    value={(btns && btns[0] && btns[0].msg) ? btns[0].msg : ''}
                    placeholder='Name button 1'
                    onChange={(e) => createNodeObj(e, idNode, nodeType)}
                />
                <Input
                    type='text'
                    data-btn='0'
                    name='saveAsKey'
                    value={(btns && btns[0] && btns[0].saveAsKey) ? btns[0].saveAsKey : ''}
                    placeholder='Save As Key Btn'
                    onChange={(e) => createNodeObj(e, idNode, nodeType)}
                />
                <Input
                    type='text'
                    data-btn='1'
                    name='msgBtn'
                    value={(btns && btns[1] && btns[1].msg) ? btns[1].msg : ''}
                    placeholder='Name button 2'
                    onChange={(e) => createNodeObj(e, idNode, nodeType)}
                />
                <Input
                    type='text'
                    data-btn='1'
                    name='saveAsKey'
                    value={(btns && btns[1] && btns[1].saveAsKey) ? btns[1].saveAsKey : ''}
                    placeholder='Save As Key Btn 2'
                    onChange={(e) => createNodeObj(e, idNode, nodeType)}
                />
            </>
            )
        }
    }
    return (
        <div>
            {className
            ?
                <MdCreate onClick={toggle} fontSize="20px" />
                :
                <Button color="primary" onClick={toggle}>{buttonLabel}</Button>
            }

            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>Modal title</ModalHeader>
                <ModalBody>
                    <Input
                        type="select"
                        id="nodeType"
                        name="nodeType"
                        defaultValue={nodeType}
                        disabled={className==='edit'}
                        onChange={changeHandler}
                    >
                        <option value="0">
                            Select an option
                        </option>
                        <option value="1">
                            Message
                        </option>
                        <option value="2">
                            Input
                        </option>
                        <option value="3">
                            Decision buttons
                        </option>
                    </Input>
                    {renderTypeNode()}
                <br />
                </ModalBody>
                <ModalFooter>
                    {className==='edit'
                    ?
                        <Button color="primary" onClick={toggle}>Done</Button>
                    :
                    <>
                        <Button
                            color="primary"
                            id='addproject'
                            onClick={e => createNode(e, toggle)}>
                            Create
                        </Button>{' '}
                        <Button color="secondary" onClick={toggle}>Cancel</Button>
                    </>
                    }

                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ModalCustom;